<?php

namespace App\Repository;

use App\Entity\Equipo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Equipo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Equipo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Equipo[]    findAll()
 * @method Equipo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EquipoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Equipo::class);
    }

//    /**
//     * @return Equipo[] Returns an array of Equipo objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    
    public function countEstado($estado)
    {
        return $this->createQueryBuilder('e')
            ->select('count(e.id)')
            ->where('e.estado = :estado')
            ->setParameter("estado",$estado)
            ->getQuery()
            ->getSingleScalarResult();
        ;
    }

    public function findOneByIMEI($imei){
        return $this->createQueryBuilder('e')
            ->andWhere('e.imei = :imei')
            ->setParameter("imei",$imei)
            ->getQuery()
            ->getOneOrNullResult();
        ;
    }

    public function countPendientes($fecha,$estado){
        return $this->createQueryBuilder('e')
        ->select('count(e.id)')
        ->where('e.fechaIngreso <= :fecha')
        ->andWhere('e.estado = :estado')
        ->setParameter("fecha", $fecha)
        ->setParameter("estado", $estado)
        ->getQuery()
            ->getSingleScalarResult();
        ;
    }
    
}
