<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180807003707 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE linea ADD plan_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE linea ADD CONSTRAINT FK_BCB8FDDEE899029B FOREIGN KEY (plan_id) REFERENCES plan (id)');
        $this->addSql('CREATE INDEX IDX_BCB8FDDEE899029B ON linea (plan_id)');
        $this->addSql('ALTER TABLE plan DROP FOREIGN KEY FK_DD5A5B7D54923972');
        $this->addSql('DROP INDEX IDX_DD5A5B7D54923972 ON plan');
        $this->addSql('ALTER TABLE plan DROP lineas_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE linea DROP FOREIGN KEY FK_BCB8FDDEE899029B');
        $this->addSql('DROP INDEX IDX_BCB8FDDEE899029B ON linea');
        $this->addSql('ALTER TABLE linea DROP plan_id');
        $this->addSql('ALTER TABLE plan ADD lineas_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plan ADD CONSTRAINT FK_DD5A5B7D54923972 FOREIGN KEY (lineas_id) REFERENCES linea (id)');
        $this->addSql('CREATE INDEX IDX_DD5A5B7D54923972 ON plan (lineas_id)');
    }
}
