<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180804232834 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE empleado (id INT AUTO_INCREMENT NOT NULL, rut VARCHAR(255) NOT NULL, nombre VARCHAR(255) NOT NULL, fecha_ingreso VARCHAR(255) NOT NULL, code_ceco VARCHAR(255) NOT NULL, nombre_ceco VARCHAR(255) NOT NULL, unidad_organizativa VARCHAR(255) NOT NULL, division VARCHAR(255) NOT NULL, subdivision VARCHAR(255) NOT NULL, cargo VARCHAR(255) NOT NULL, empresa VARCHAR(255) NOT NULL, nombre_jefatura VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equipo (id INT AUTO_INCREMENT NOT NULL, modelo_id INT DEFAULT NULL, imei VARCHAR(255) NOT NULL, fecha_ingreso VARCHAR(255) NOT NULL, INDEX IDX_C49C530BC3A9576E (modelo_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE grupo (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE linea (id INT AUTO_INCREMENT NOT NULL, empleado_id INT DEFAULT NULL, grupo_id INT DEFAULT NULL, equipo_id INT DEFAULT NULL, numero INT NOT NULL, INDEX IDX_BCB8FDDE952BE730 (empleado_id), INDEX IDX_BCB8FDDE9C833003 (grupo_id), INDEX IDX_BCB8FDDE23BFBED (equipo_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE modelo (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, codigo VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuario (id INT AUTO_INCREMENT NOT NULL, rol_id INT DEFAULT NULL, nombre VARCHAR(255) NOT NULL, apellido VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, INDEX IDX_2265B05D4BAB96C (rol_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE equipo ADD CONSTRAINT FK_C49C530BC3A9576E FOREIGN KEY (modelo_id) REFERENCES modelo (id)');
        $this->addSql('ALTER TABLE linea ADD CONSTRAINT FK_BCB8FDDE952BE730 FOREIGN KEY (empleado_id) REFERENCES empleado (id)');
        $this->addSql('ALTER TABLE linea ADD CONSTRAINT FK_BCB8FDDE9C833003 FOREIGN KEY (grupo_id) REFERENCES grupo (id)');
        $this->addSql('ALTER TABLE linea ADD CONSTRAINT FK_BCB8FDDE23BFBED FOREIGN KEY (equipo_id) REFERENCES equipo (id)');
        $this->addSql('ALTER TABLE usuario ADD CONSTRAINT FK_2265B05D4BAB96C FOREIGN KEY (rol_id) REFERENCES role (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE linea DROP FOREIGN KEY FK_BCB8FDDE952BE730');
        $this->addSql('ALTER TABLE linea DROP FOREIGN KEY FK_BCB8FDDE23BFBED');
        $this->addSql('ALTER TABLE linea DROP FOREIGN KEY FK_BCB8FDDE9C833003');
        $this->addSql('ALTER TABLE equipo DROP FOREIGN KEY FK_C49C530BC3A9576E');
        $this->addSql('ALTER TABLE usuario DROP FOREIGN KEY FK_2265B05D4BAB96C');
        $this->addSql('DROP TABLE empleado');
        $this->addSql('DROP TABLE equipo');
        $this->addSql('DROP TABLE grupo');
        $this->addSql('DROP TABLE linea');
        $this->addSql('DROP TABLE modelo');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE usuario');
    }
}
