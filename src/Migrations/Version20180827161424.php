<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180827161424 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE movimiento ADD empleado_origen_id INT NOT NULL, ADD fecha DATETIME NOT NULL');
        $this->addSql('ALTER TABLE movimiento ADD CONSTRAINT FK_C8FF107AD3FB4D48 FOREIGN KEY (empleado_origen_id) REFERENCES empleado (id)');
        $this->addSql('CREATE INDEX IDX_C8FF107AD3FB4D48 ON movimiento (empleado_origen_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE movimiento DROP FOREIGN KEY FK_C8FF107AD3FB4D48');
        $this->addSql('DROP INDEX IDX_C8FF107AD3FB4D48 ON movimiento');
        $this->addSql('ALTER TABLE movimiento DROP empleado_origen_id, DROP fecha');
    }
}
