<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
class UserFixture extends Fixture
{
    private $passwordEncoder;
    
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $user = new User();
        $user->setEmail('bastian@raion.cl');
        $user->setNombre('Bastian');
        $user->setApellido('Vergara');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'Soprole123'
        ));
        $user->setRoles(array("ROLE_ADMIN","ROLE_TEC"));
        $manager->persist($user);
            
        $manager->flush();
        
        $user = new User();
        $user->setEmail('catherine.barahona@soprole.cl');
        $user->setNombre('Catherine');
        $user->setApellido('Barahona');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'Soprole123'
        ));
        $user->setRoles(array("ROLE_TEC"));
        $manager->persist($user);
            
        $manager->flush();
        
        $user = new User();
        $user->setEmail('mesadeayuda@soprole.cl');
        $user->setNombre('HelpDesk');
        $user->setApellido('Admin');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'MDA5100.'
        ));
        $user->setRoles(array("ROLE_ADMIN","ROLE_TEC"));
        $manager->persist($user);
            
        $manager->flush();
    }
}
