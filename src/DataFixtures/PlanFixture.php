<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Plan;

class PlanFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $plan = new Plan();
        $plan->setCodigo("XKG");
        $plan->setEstado(1);
        $plan->setCosto(15958);
        $plan->setMinutos(500);
        $plan->setNombre("Multimedia Empresas XS Especial CE");
        $plan->setUmbral(9000);
        
        $manager->persist($plan);
        $manager->flush();

        $plan = new Plan();
        $plan->setCodigo("XKL");
        $plan->setEstado(1);
        $plan->setCosto(21000);
        $plan->setMinutos(50000);
        $plan->setNombre("Multimedia Empresas S Especial CE");
        $plan->setUmbral(18000);

        $manager->persist($plan);
        $manager->flush();

        $plan = new Plan();
        $plan->setCodigo("XKP");
        $plan->setEstado(1);
        $plan->setCosto(25202);
        $plan->setMinutos(50000);
        $plan->setNombre("Multimedia Empresas M Especial CE");
        $plan->setUmbral(25000);

        $manager->persist($plan);
        $manager->flush();

        $plan = new Plan();
        $plan->setCodigo("XLR");
        $plan->setEstado(1);
        $plan->setCosto(38647);
        $plan->setMinutos(50000);
        $plan->setNombre("Multimedia Empresas Libre Especial CE");
        $plan->setUmbral(-1);

        $manager->persist($plan);
        $manager->flush();
    }
}
