<?php

namespace App\Controller;

use App\Entity\Modelo;
use App\Form\ModeloType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Plan;
use App\Service\excelManagementService;
use App\Service\FileUploader;
use App\Form\ImportExcelType;

/**
 * @Route("/modelo")
 */
class ModeloController extends Controller
{
    /**
     * @Route("/", name="modelo_index", methods="GET")
     */
    public function index(): Response
    {
     
        $modelos = $this->getDoctrine()->getRepository(Modelo::class)->findAll();
        return $this->render('modelo/index.html.twig', array (
            'modelos' => $modelos,
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'modelo_index',
                  'title' => 'Modelo',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
         ));
    }

    /**
     * @Route("/new", name="modelo_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
     

        $modelo = new Modelo();
        $form = $this->createForm(ModeloType::class, $modelo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $modelo = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($modelo);
            $em->flush();
            $this->addFlash("Exito","Se ha creado nuevo modelo correctamente"); 
            return $this->redirectToRoute('administracion',array("active"=>"modelos"));
        }

        return $this->render('modelo/new.html.twig', [
            'modelo' => $modelo,
            'form' => $form->createView(),
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'modelo_index',
                  'title' => 'Modelo',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
        ]);
    }

    /**
     * @Route("/{id}", name="modelo_show", methods="GET")
     */
    public function show(Modelo $modelo): Response
    {
      if($modelo != null){
            return $this->render('modelo/show.html.twig', ['modelo' => $modelo,
                'page' => array(
                    'title' => '',
                    'description' => '',
                    'module' => array(
                    'name' => 'modelo_index',
                    'title' => 'Modelo',
                    'icon' => 'fa-list-alt'
                    ),
    
                ),
            ]);
        }else{
            $this->addFlash("error","No se encontro el modelo");
            return $this->redirectToRoute('administracion',array("active"=>"modelos"));
        }
    }

    /**
     * @Route("/{id}/edit", name="modelo_edit", methods="GET|POST")
     */
    public function edit(Modelo $modelo, Request $request): Response
    {
      
            $form = $this->createForm(ModeloType::class, $modelo);
            $form->handleRequest($request);
       
            
        if ($form->isSubmitted() && $form->isValid()) {
            $modelo = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($modelo);
            $em->flush();
            $this->addFlash("Exito","Se ha modificado modelo correctamente"); 
            return $this->redirectToRoute('administracion',array("active"=>"modelos"));
         
        }

        return $this->render('modelo/edit.html.twig', [
            'modelo' => $modelo,
            'form' => $form->createView(),
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'modelo_index',
                  'title' => 'Modelo',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
        ]);
    }

    /**
     * @Route("/{id}", name="modelo_delete", methods="DELETE")
     */
    public function delete(Modelo $modelo, Request $request): Response
    {
        if ($modelo != null){
            if ($this->isCsrfTokenValid('delete'.$modelo->getId(), $request->request->get('_token'))) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($modelo);
                $em->flush();
                $this->addFlash("Exito","Se ha eliminado correctamente el modelo");
            }
        }else{
            $this->addFlash("Exito","No se encontró ningún modelo con ese ID");
        }

        return $this->redirectToRoute('administracion',array("active"=>"modelos"));
    }

    
    /**
     * @Route("/import/modelo",name="importExcelModelo")
     */
    public function import(Request $request, excelManagementService $excel, FileUploader $fileUploader){

        $form = $this->createForm(ImportExcelType::class,array(
            'action' => $this->generateUrl('importExcelModelo'),
        ));

        $form->handleRequest($request);
        
        $modelos = array();
        if ($form->isSubmitted() && $form->isValid()){
            $file = $form["file"]->getData();
            $fileName = $fileUploader->upload($file);
            $archivo = $excel->readExcel("uploads/imports/import.xlsx");
            $items = count($archivo);
            $editados = 0;
            $creados = 0;
            $count = 0;
            foreach ($archivo as $row){
                if ($count == 0){
                    $count++;
                }else{
                    
                    $modelo = $this->getDoctrine()->getRepository(Modelo::class)->findOneByCodigoDescuento($row[2]);
                    
                    $em = $this->getDoctrine()->getManager();
                    if ($modelo != null){
                        $modelo->setNombre($row[0]);
                        $modelo->setCodigo($row[1]);
                        $modelo->setCodDescuento($row[2]);
                        $modelo->setDescuento($row[3]);
                        $plan = $this->getDoctrine()->getRepository(Plan::class)->findOneByCodigo($row[4]);
                        $modelo->setPlan($plan);
                        $modelo->setPrecioVenta($row[5]);
                        $modelo->setCuotaInicial($row[6]);
                        $modelo->setCuotaArrendamiento($row[7]);
                        $em->persist($modelo);
                        $modelos[] = $modelo;
                        $editados++;
                    }else{
                        $modelo = new Modelo();
                        $modelo->setNombre($row[0]);
                        $modelo->setCodigo($row[1]);
                        $modelo->setCodDescuento($row[2]);
                        $modelo->setDescuento($row[3]);
                        $plan = $this->getDoctrine()->getRepository(Plan::class)->findOneByCodigo($row[4]);
                        $modelo->setPlan($plan);
                        $modelo->setPrecioVenta($row[5]);
                        $modelo->setCuotaInicial($row[6]);
                        $modelo->setCuotaArrendamiento($row[7]);
                        $em->persist($modelo);
                        $modelos[] = $modelo;
                        
                        $creados++;
                    }
                    $em->flush();
                }
              }
              $this->addFlash("Exito","Se han creado ".$creados." modelos y modificado ".$editados." de ".$items." items importados.");
              
        }
        return $this->redirectToRoute("administracion",["active"=>"modelos"]);
    }
}