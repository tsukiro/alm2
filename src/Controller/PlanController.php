<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Plan;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\PlanType;

class PlanController extends Controller
{
    /**
     * @Route("/plan", name="plan", methods="GET")
     */
    public function index()
    {
        $planes = $this->getDoctrine()->getRepository(Plan::class)->findAll();

        return $this->render('plan/index.html.twig', [
            'plan' => $plan,
            'controller_name' => 'PlanController',
        ]);
    }

    /**
     * @Route("/plan/new", name="plan_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $plan = new Plan();
        $form = $this->createForm(PlanType::class,$plan);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $plan = $form->getData();
            $plan->setEstado(1);
            $em = $this->getDoctrine()->getManager();
            $em->persist($plan);
            $em->flush();
        
            $this->addFlash("Exito","Se ha creado nuevo plan correctamente"); 
            return $this->redirectToRoute('administracion',array("active"=>"planes"));
        }
        return $this->render('plan/new.html.twig', [
            'plan' => $plan,
            'form' => $form->createView(),
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'plan_index',
                  'title' => 'Plan',
                  'icon' => 'fa-list-alt'
                ),
              ),
        ]);
    }
    /**
     * @Route("/plan/show/{id}", name="plan_show", methods="GET")
     */
    public function show(Plan $plan){
        if ($plan != null){
            return $this->render('plan/show.html.twig', ['plan' => $plan,
                'page' => array(
                    'title' => '',
                    'description' => '',
                    'module' => array(
                    'name' => 'plan_index',
                    'title' => 'Plan',
                    'icon' => 'fa-list-alt'
                    ),
                ),
            ]);
        }else{
            $this->addFlash("error","No se encontro el plan");
            return $this->redirectToRoute('administracion',array("active"=>"planes"));
        }
    }
    /**
     * @Route("/plan/edit/{id}", name="plan_edit", methods="GET|POST")
     */
    public function edit(Plan $plan, Request $request){
        if ($plan != null){
            $form = $this->createForm(PlanType::class, $plan);
            $form->handleRequest($request);
        }
        if ($form->isSubmitted() && $form->isValid()) {
            ($plan = $form->getData());
            $em = $this->getDoctrine()->getManager();
            $em->persist($plan);
            $em->flush();
            $this->addFlash("Exito","Se ha modificado plan correctamente"); 
            return $this->redirectToRoute('administracion',array("active"=>"planes"));
            //$this->addFlash("Error","Hubo un error al modificar la entidad.");
            //return $this->redirectToRoute('rol_index');
        }
        return $this->render('plan/edit.html.twig', [
            'plan' => $plan,
            'form' => $form->createView(),
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'plan_index',
                  'title' => 'Plan',
                  'icon' => 'fa-list-alt'
                ),
              ),
        ]);
    }
    /**
     * @Route("/plan/delete/{id}", name="plan_delete", methods="DELETE")
     */
    public function delete(Plan $plan, Request $request){
        if ($plan != null){
            if ($this->isCsrfTokenValid('delete'.$plan->getId(), $request->request->get('_token'))) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($plan);
                $em->flush();
                    $this->addFlash("Exito","Se ha eliminado correctamente el plan");
                   // $this->addFlash("Exito","No se encontró ningún rol con ese ID");
            }
        }
        return $this->redirectToRoute('administracion',array("active"=>"planes"));
    }
}
