<?php

namespace App\Controller;


use App\Entity\Equipo;
use App\Form\EquipoType;
use App\Entity\Modelo;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\excelManagementService;
use App\Service\FileUploader;
use App\Form\ImportExcelType;

/**
 * @Route("/equipo")
 */
class EquipoController extends Controller
{
    /**
     * @Route("/", name="equipo_index", methods="GET")
     */
    public function index(): Response
    {
     
        $equipos = $this->getDoctrine()->getRepository(Equipo::class)->findAll();

        return $this->render('equipo/index.html.twig', array (
            'equipos' => $equipos,
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'equipo_index',
                  'title' => 'Equipo',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
            
            
         ));
    }

    /**
     * @Route("/new", name="equipo_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $equipo = new Equipo();
        $now = (new \Datetime('now'));
        
        $equipo->setFechaIngreso($now->format('Y-m-d'));
        $form = $this->createForm(EquipoType::class, $equipo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $equipo = $form->getData();
            $equipo->setFechaIngreso(new \DateTime($equipo->getFechaIngreso()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($equipo);
            $em->flush();
            
            $this->addFlash("Exito","Se ha creado nuevo equipo correctamente"); 
                return $this->redirectToRoute('gestion',array("active"=>"equipos"));
            
            //$this->addFlash("Error","No se la logrado crear el nuevo equipo.");
            //return $this->redirectToRoute('equipo_index');
            
        }

        return $this->render('equipo/new.html.twig', [
            'equipo' => $equipo,
            'form' => $form->createView(),
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'equipo_index',
                  'title' => 'Equipo',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
        ]);
    }

    /**
     * @Route("/{id}", name="equipo_show", methods="GET")
     */
    public function show(Equipo $equipo): Response
    {
        if ($equipo != null){
            return $this->render('equipo/show.html.twig', 
            ['equipo' => $equipo,
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'equipo_index',
                  'title' => 'Equipo',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
            ]);
        }else {
            $this->addFlash("error","No se encontro el equipo");
             return $this->redirectToRoute('gestion',array("active"=>"equipos"));
        }
    }

    /**
     * @Route("/{id}/edit", name="equipo_edit", methods="GET|POST")
     */
    public function edit(Equipo $equipo, Request $request): Response
    {
        if ($equipo != null){
            $equipo->setFechaIngreso($equipo->getFechaIngreso()->format('Y-m-d'));
            $form = $this->createForm(EquipoType::class, $equipo);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $equipo->setFechaIngreso(new \DateTime($equipo->getFechaIngreso()));

                $em = $this->getDoctrine()->getManager();
                $em->persist($equipo);
                $em->flush();
                $this->addFlash("Exito","Se ha modificado el equipo correctamente");
                return $this->redirectToRoute('gestion',array("active"=>"equipos"));
            }

            return $this->render('equipo/edit.html.twig', [
                'equipo' => $equipo,
                'form' => $form->createView(),
                'page' => array(
                    'title' => '',
                    'description' => '',
                    'module' => array(
                    'name' => 'equipo_index',
                    'title' => 'Equipo',
                    'icon' => 'fa-list-alt'
                    ),
    
                ),
            ]);
        }
    }

    /**
     * @Route("/{id}", name="equipo_delete", methods="DELETE")
     */
    public function delete(Equipo $equipo, Request $request): Response
    {       
        if ($this->isCsrfTokenValid('delete'.$equipo->getId(), $request->request->get('_token'))) {
            
            $em = $this->getDoctrine()->getManager();
            $em->remove($equipo);
            $em->flush();
            $this->addFlash("Exito","Se ha eliminado el equipo correctamente");
        }
        return $this->redirectToRoute('gestion',array("active"=>"equipos"));
    }

    
    /**
     * @Route("/import/equipo",name="importExcelEquipo")
     */
    public function import(Request $request, excelManagementService $excel, FileUploader $fileUploader){

        $form = $this->createForm(ImportExcelType::class,array(
            'action' => $this->generateUrl('importExcelEquipo'),
        ));

        $form->handleRequest($request);
        
        $equipos = array();
        if ($form->isSubmitted() && $form->isValid()){
            $file = $form["file"]->getData();
            $fileName = $fileUploader->upload($file);
            $archivo = $excel->readExcel("uploads/imports/import.xlsx");
            $items = count($archivo);
            $editados = 0;
            $creados = 0;
            $count = 0;
            foreach ($archivo as $row){
                if ($count == 0){
                    $count++;
                }else{
                    
                    $equipo = $this->getDoctrine()->getRepository(Equipo::class)->findOneByIMEI($row[0]);
                    
                    $em = $this->getDoctrine()->getManager();
                    if ($equipo != null){
                        $equipo->setImei($row[0]);
                        $equipo->setFechaIngreso(new \DateTime($row[1]));
                        $equipo->setModelo($this->getDoctrine()->getRepository(Modelo::class)->findOneByCodigoDescuento($row[2]));
                        $equipo->setEstado($row[3]);
                        $em->persist($equipo);
                        $equipos[] = $equipo;
                        $editados++;
                    }else{
                        $equipo = new Equipo();
                        $equipo->setImei($row[0]);
                        $equipo->setFechaIngreso(new \DateTime($row[1]));
                        $equipo->setModelo($this->getDoctrine()->getRepository(Modelo::class)->findOneByCodigoDescuento($row[2]));
                        $equipo->setEstado($row[3]);
                        $em->persist($equipo);
                        $equipos[] = $equipo;
                        
                        $creados++;
                    }
                    $em->flush();
                }
              }
              $this->addFlash("Exito","Se han creado ".$creados." equipos y modificado ".$editados." de ".$items." items importados.");
              
        }
        return $this->redirectToRoute("administracion",["active"=>"equipos"]);
    }
}
