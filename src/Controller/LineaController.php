<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\CeCoType;
use App\Form\InformeEmpleadoType;
use App\Form\InformeLineasType;
use App\Form\ImportExcelType;
use App\Entity\Linea;
use App\Entity\Equipo;
use App\Entity\Movimiento;
use App\Form\LineaType;
use App\Form\ReAsignacionLineaType;
use App\Form\CambioNumeroType;
use App\Form\CambioEquipoType;
use App\Entity\Empleado;
use App\Service\excelManagementService;
use App\Service\FileUploader;
use App\Entity\Grupo;
use App\Entity\Plan;

/**
 * @Route("/lineas")
 */
class LineaController extends Controller
{
    /**
     * @Route("/", name="linea_index")
     */
    public function index(Request $request)
    {
        $lineas = $this->getDoctrine()->getRepository(Linea::class)->findAll();
        return $this->render('linea/index.html.twig', [
            'lineas' => $lineas,
            'controller_name' => 'Linea',
        ]);
    }
    /**
     * @Route("/new", name="linea_new")
     */
    public function new(Request $request){
        $linea = new Linea();
        $form = $this->createForm(LineaType::class, $linea);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($linea);
            $em->flush();
            $movimiento = new Movimiento();
            $movimiento->setEmpleado($linea->getEmpleado());
            $movimiento->setEmpleadoOrigen($linea->getEmpleado());
            $movimiento->setTipo(0);
            $movimiento->setLinea($linea);
            $movimiento->setEstado(1);
            $movimiento->setFecha(new \DateTime('now'));
            $movimiento->setTecnico($this->getUser());
            $em->persist($movimiento);
            $em->flush();
            $this->addFlash("Exito","Se agrega la nueva linea exitosamente");
            return $this->redirectToRoute('gestion',array('active'=>'lineas'));
        }

        return $this->render('linea/new.html.twig', [
            'form' => $form->createView(),
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'linea_index',
                  'title' => 'Linea',
                  'icon' => 'fa-list-alt'
                ),
              ),
        ]);
    }
    /**
     * @Route("/edit/{id}", name="linea_edit")
     */
    public function edit(Linea $linea, Request $request){
        $form = $this->createForm(LineaType::class,$linea);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($linea);
            $em->flush();
            $this->addFlash("Exito","Se modifica la linea exitosamente");
            return $this->redirectToRoute('gestion',array('active'=>'lineas'));
        }

        return $this->render("linea/edit.html.twig",[
            "form" => $form->createView(),
            "linea" => $linea,
        ]);
    }
    /**
     * @Route("/view/{id}", name="linea_view")
     */
    public function view(Linea $linea, Request $request){
        $empleadoAnterior = $linea->getempleado();
        $numeroAnterior = $linea->getNumero();
        $reasignarForm = $this->createForm(ReAsignacionLineaType::class);
        $cambioNumeroForm = $this->createForm(CambioNumeroType::class,$linea);
        $cambioEquipoForm = $this->createForm(CambioEquipoType::class);

        $reasignarForm->handleRequest($request);
        $cambioNumeroForm->handleRequest($request);
        $cambioEquipoForm->handleRequest($request);
        
        if ($reasignarForm->isSubmitted() && $reasignarForm->isValid()){
            $rut = $reasignarForm->get("empleado")->getData();
            $empleado = $this->getDoctrine()->getRepository(Empleado::class)->findOneBy(array("rut"=>$rut));
            if ($empleado != null){
                $linea->setEmpleado($empleado);
                $em = $this->getDoctrine()->getManager();
                $em->persist($linea);
                $em->flush();
                $movimiento = new Movimiento();
                $movimiento->setEmpleado($linea->getEmpleado());
                $movimiento->setEmpleadoOrigen($empleadoAnterior);
                $movimiento->setTipo(1);
                $movimiento->setLinea($linea);
                $movimiento->setEstado(1);
                $movimiento->setFecha(new \DateTime('now'));
                $movimiento->setObservacion("Se reasigna linea ".$linea->getNumero()." a ".$linea->getEmpleado()->getNombre());
                $movimiento->setTecnico($this->getUser());
                $em->persist($movimiento);
                $em->flush();
                $this->addFlash("Exito","Se reasigna la linea exitosamente");
            }else{
                $this->addFlash("Error","El empleado al que deseas asignar la linea no se encuentra en la lista de empleados.");
            }
            return $this->redirectToRoute('linea_view',array('id'=>$linea->getId()));
        }
        if ($cambioNumeroForm->isSubmitted() && $cambioNumeroForm->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($linea);
            $em->flush();
            $movimiento = new Movimiento();
            $movimiento->setEmpleado($linea->getEmpleado());
            $movimiento->setEmpleadoOrigen($empleadoAnterior);
            $movimiento->setTipo(7);
            $movimiento->setLinea($linea);
            $movimiento->setEstado(1);
            $movimiento->setFecha(new \DateTime('now'));
            $movimiento->setObservacion("Se cambia el numero ".$numeroAnterior." de la linea a ".$linea->getNumero());            
            $movimiento->setTecnico($this->getUser());
            $em->persist($movimiento);
            $em->flush();
            $this->addFlash("Exito","Se modifica la linea exitosamente");
            return $this->redirectToRoute('linea_view',array('id'=>$linea->getId()));
        }
        if ($cambioEquipoForm->isSubmitted() && $cambioEquipoForm->isValid()){
            $imei = $cambioEquipoForm->get('equipo')->getData();
            $equipo = $this->getDoctrine()->getRepository(Equipo::class)->findOneBy(array("imei" => $imei));
            if ($equipo != null){
                $linea->setEquipo($equipo);
                $em = $this->getDoctrine()->getManager();
                $em->persist($linea);
                $em->flush();
                $movimiento = new Movimiento();
                $movimiento->setEmpleado($linea->getEmpleado());
                $movimiento->setEmpleadoOrigen($empleadoAnterior);
                $movimiento->setTipo(6);
                $movimiento->setLinea($linea);
                $movimiento->setEstado(1);
                $movimiento->setFecha(new \DateTime('now'));
                $movimiento->setObservacion("Se cambia el Equipo de la linea".$numeroAnterior." a ".$linea->getEquipo()->displayName());
                $movimiento->setTecnico($this->getUser());
                $em->persist($movimiento);
                $em->flush();
                $this->addFlash("Exito","Se modifica la linea exitosamente");
            }else{
                $this->addFlash("Error","El imei del equipo indicado no existe.");

            }
          
            return $this->redirectToRoute('linea_view',array('id'=>$linea->getId()));
        }

        return $this->render('linea/show.html.twig', [
            'linea' => $linea,
            'form' => array(
                "reasignacion" => $reasignarForm->createView(),
                "cambioNumero" => $cambioNumeroForm->createView(),
                "cambioEquipo" => $cambioEquipoForm->createView(),
            ),
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'linea_index',
                  'title' => 'Linea',
                  'icon' => 'fa-list-alt'
                ),
              ),
        ]);
    }

    /**
     * @Route("/{id}", name="linea_delete", methods="DELETE")
     */
    public function delete(Request $request,Linea $linea): Response
    {   
        if ($this->isCsrfTokenValid('delete'.$linea->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($linea);
            $em->flush();
            
            $this->addFlash("Exito","Se ha eliminado la linea correctamente");
            
              //  $this->addFlash("Error","Ha habido un error al eliminar la linea");   
        }
        return $this->redirectToRoute('gestion',["active"=>"lineas"]);
    }

    /**
     * @Route("/import/linea",name="importExcelLinea")
     */
    public function import(Request $request, excelManagementService $excel, FileUploader $fileUploader){

        $form = $this->createForm(ImportExcelType::class,array(
            'action' => $this->generateUrl('importExcelLinea'),
        ));

        $form->handleRequest($request);
        
        $lineas = array();
        if ($form->isSubmitted() && $form->isValid()){
            $file = $form["file"]->getData();
            $fileName = $fileUploader->upload($file);
            $archivo = $excel->readExcel("uploads/imports/import.xlsx");
            $items = count($archivo);
            $editados = 0;
            $creados = 0;
            $count = 0;
            foreach ($archivo as $row){
                if ($count == 0){
                    $count++;
                }else{
                    
                    $linea = $this->getDoctrine()->getRepository(Linea::class)->findOneBy(array("numero"=>$row[0]));
                    $em = $this->getDoctrine()->getManager();

                    if ($linea != null){
                        $linea->setNumero($row[0]);
                        $linea->setEmpleado($this->getDoctrine()->getRepository(Empleado::class)->findOneBy(array("rut" => $row[1])));
                        $grupo = $this->getDoctrine()->getRepository(Grupo::class)->findOneBy(array("nombre" => $row[2]));
                        if ($grupo == null){
                            $grupo = new Grupo();
                            $grupo->setNombre($row[2]);
                        }
                        $em->persist($grupo);
                        $linea->setGrupo($grupo);
                        $linea->setEquipo($this->getDoctrine()->getRepository(Equipo::class)->findOneByIMEI($row[3]));
                        $linea->setPlan($this->getDoctrine()->getRepository(Plan::class)->findOneByCodigo($row[4]));
                        $em->persist($linea);
                        $lineas[] = $linea;
                        $editados++;
                    }else{
                        $linea = new Linea();
                        $linea->setNumero($row[0]);
                        $linea->setEmpleado($this->getDoctrine()->getRepository(Empleado::class)->findOneBy(array("rut" => $row[1])));
                        $grupo = $this->getDoctrine()->getRepository(Grupo::class)->findOneBy(array("nombre" => $row[2]));
                        if ($grupo == null){
                            $grupo = new Grupo();
                            $grupo->setNombre($row[2]);
                        }
                        
                        $em->persist($grupo);
                        $linea->setGrupo($grupo);
                        $linea->setEquipo($this->getDoctrine()->getRepository(Equipo::class)->findOneByIMEI($row[3]));
                        $linea->setPlan($this->getDoctrine()->getRepository(Plan::class)->findOneByCodigo($row[4]));
                        $em->persist($linea);
                        $lineas[] = $linea;
                        
                        $creados++;
                    }
                    $em->flush();
                }
              }
              $this->addFlash("Exito","Se han creado ".$creados." equipos y modificado ".$editados." de ".$items." items importados.");
              
        }
        return $this->redirectToRoute("administracion",["active"=>"equipos"]);
    }
}
