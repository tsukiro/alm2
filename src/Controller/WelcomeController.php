<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Plan;
use App\Entity\Linea;
use App\Entity\Equipo;
use App\Form\SearchInputType;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Usuario;
use App\Entity\Empleado;
class WelcomeController extends Controller
{
    /**
     * @Route("/", name="welcome")
     */
    public function index(Request $request)
    {
       $form = $this->createForm(SearchInputType::class);

       $planes = $this->getDoctrine()->getRepository(Plan::class)->findBy(array("estado" => 1));
       $lineas = $this->getDoctrine()->getRepository(Linea::class)->countAll();
       $equipos = $this->getDoctrine()->getRepository(Equipo::class)->countEstado(0);
       $now = (new \Datetime('-18months'))->format("Y-m-d H:i:s");
       $pendientes = $this->getDoctrine()->getRepository(Equipo::class)->countPendientes($now,0);
       $form->handleRequest($request);

       $searchedEmpleados = null;
       $searchedLineas = null;

        if ($form->isSubmitted() && $form->isValid()){
            $busqueda = $form->get("busqueda")->getData();

            $searchedEmpleados = $this->getDoctrine()->getRepository(Empleadodo::class)->findByRut($busqueda);
            $searchedLineas = $this->getDoctrine()->getRepository(Linea::class)->findBy(array("numero"=>$busqueda));

        }

        return $this->render('welcome/index.html.twig', [
            "dashboard" => array(
                "lineas" => $lineas,
                "equipos" => $equipos,
                "pendientes" => $pendientes,
            ),
            "form" => $form->createView(),
            "planes" => $planes,
            "resultados" => array("empleados" => $searchedEmpleados, "lineas" => $searchedLineas),
            'controller_name' => 'WelcomeController',
        ]);
    }
}
