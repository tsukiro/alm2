<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\CeCoType;
use App\Form\InformeEmpleadoType;
use App\Form\InformeLineasType;
use App\Form\ImportExcelType;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Linea;
use App\Entity\Empleado;
use App\Service\excelManagementService;
use App\Service\dateManager;


class InformeController extends Controller
{
    /**
     * @Route("/informe", name="informe")
     */
    public function index(Request $request,excelManagementService $excel, dateManager $date)
    {
        
        $ceCoForm = $this->createForm(CeCoType::class);

        $ceCoForm->handleRequest($request);

        $empleadoForm = $this->createForm(InformeEmpleadoType::class);

        $empleadoForm->handleRequest($request);

        $lineasForm = $this->createForm(InformeLineasType::class);

        $lineasForm->handleRequest($request);

        if($empleadoForm->isSubmitted() && $empleadoForm->isValid()){
            $valor = $empleadoForm->get("valor")->getData();
            $empleado = $this->getDoctrine()->getRepository(Empleado::class)->findByRut($valor);
            if($empleado != null){
                $lineas = $empleado->getLineas();
                $l = array();
                $l[] = array(
                    "Numero",
                    "Rut",
                    "Usuario",
                    "Antiguedad",
                    "Modelo",
                    "CeCo",
                    "Cod. Plan",
                    "Plan",
                    "Costo Mensual",
                    "IMEI",
                    "Grupo",
                    "Restantes",
                    "Costo Renovación"
                );
                foreach ($lineas as $linea){
                    $antiguedad = (($linea->getEquipo()) !== null) ? $date->get_month_diff($linea->getEquipo()->getFechaIngreso()) : 0;

                    if (($antiguedad - 18)<=0){
                        $restantes = 18 - $antiguedad ; 
                    }else{
                        $restantes = 0;  
                    }
                    $l[] = array(
                        $linea->getNumero(),
                        
                        ($linea->getEmpleado() != null) ? $linea->getEmpleado()->getRut() : "Empleado no asociado",
                        ($linea->getEmpleado() != null) ? $linea->getEmpleado()->getNombre() : "Empleado no asociado",
                        $antiguedad,
                        ($linea->getEquipo() != null) ? $linea->getEquipo()->getModelo()->getNombre() : "Sin equipo asociado",
                        ($linea->getEmpleado() != null) ? $linea->getEmpleado()->getCodeCeco() : "Empleado no asociado",
                        ($linea->getPlan() != null) ? $linea->getPlan()->getCodigo() : "Sin plan asociado",
                        ($linea->getPlan() != null) ? $linea->getPlan()->getNombre() : "Sin plan asociado",
                        ($linea->getPlan() != null) ? $linea->getPlan()->getCosto() : "Sin plan asociado",
                        ($linea->getEquipo() != null) ? $linea->getEquipo()->getImei() : "Sin equipo asociado",
                        ($linea->getGrupo() != null) ? $linea->getGrupo()->getNombre() : "General",
                        $restantes,
                        $restantes * (($linea->getEquipo() != null) ? $linea->getEquipo()->getModelo()->getCuotaArrendamiento() : 0 ) 
                    );
                }
                $excel->writeExcel($l);
                $this->addFlash("Exito","Se creó informe <a target='_blank' href='uploads/writer/export.xlsx' >Click Aqui</a> para descargar.");
            }else{
                $this->addFlash("Error","No se encontró el empleado buscado");
            }
        }

        if($ceCoForm->isSubmitted() && $ceCoForm->isValid()){
            $valor = $ceCoForm->get("valor")->getData();
            $lineas = $this->getDoctrine()->getRepository(Linea::class)->findAll();
                $l = array();
                $l[] = array(
                    "Numero",
                    "Rut",
                    "Usuario",
                    "Antiguedad",
                    "Modelo",
                    "CeCo",
                    "Cod. Plan",
                    "Plan",
                    "Costo Mensual",
                    "IMEI",
                    "Grupo",
                    "Restantes",
                    "Costo Renovación"
                );
                foreach ($lineas as $linea){
                    if ($linea->getEmpleado() != null){
                        if ($linea->getEmpleado()->getCodeCeco() == $valor){
                            $antiguedad = (($linea->getEquipo()) !== null) ? $date->get_month_diff($linea->getEquipo()->getFechaIngreso()) : 0;

                            if (($antiguedad - 18)<=0){
                                $restantes = 18 - $antiguedad ; 
                            }else{
                                $restantes = 0;  
                            }
                            $l[] = array(
                                $linea->getNumero(),
                                
                                ($linea->getEmpleado() != null) ? $linea->getEmpleado()->getRut() : "Empleado no asociado",
                                ($linea->getEmpleado() != null) ? $linea->getEmpleado()->getNombre() : "Empleado no asociado",
                                $antiguedad,
                                ($linea->getEquipo() != null) ? $linea->getEquipo()->getModelo()->getNombre() : "Sin equipo asociado",
                                ($linea->getEmpleado() != null) ? $linea->getEmpleado()->getCodeCeco() : "Empleado no asociado",
                                ($linea->getPlan() != null) ? $linea->getPlan()->getCodigo() : "Sin plan asociado",
                                ($linea->getPlan() != null) ? $linea->getPlan()->getNombre() : "Sin plan asociado",
                                ($linea->getPlan() != null) ? $linea->getPlan()->getCosto() : "Sin plan asociado",
                                ($linea->getEquipo() != null) ? $linea->getEquipo()->getImei() : "Sin equipo asociado",
                                ($linea->getGrupo() != null) ? $linea->getGrupo()->getNombre() : "General",
                                $restantes,
                                $restantes * (($linea->getEquipo() != null) ? $linea->getEquipo()->getModelo()->getCuotaArrendamiento() : 0 ) 
                            );
                        }
                    }
                    
                }
                
                $excel = $excel->writeExcel($l);
                $this->addFlash("Exito","Se creó informe <a  target='_blank' href='uploads/writer/export.xlsx' >Click Aqui</a> para descargar.");
        }

        if($lineasForm->isSubmitted() && $lineasForm->isValid()){
            $lineas = $this->getDoctrine()->getRepository(Linea::class)->findAll();
                $l = array();
                $l[] = array(
                    "Numero",
                    "Rut",
                    "Usuario",
                    "Antiguedad",
                    "Modelo",
                    "CeCo",
                    "Cod. Plan",
                    "Plan",
                    "Costo Mensual",
                    "IMEI",
                    "Grupo",
                    "Restantes",
                    "Costo Renovación"
                );
                foreach ($lineas as $linea){
                    
                    $antiguedad = (($linea->getEquipo()) !== null) ? $date->get_month_diff($linea->getEquipo()->getFechaIngreso()) : 0;
                    
                    if (($antiguedad - 18)<=0){
                        $restantes = 18 - $antiguedad ; 
                    }else{
                        $restantes = 0;  
                    }
                    $l[] = array(
                        $linea->getNumero(),
                        
                        ($linea->getEmpleado() != null) ? $linea->getEmpleado()->getRut() : "Empleado no asociado",
                        ($linea->getEmpleado() != null) ? $linea->getEmpleado()->getNombre() : "Empleado no asociado",
                        $antiguedad,
                        ($linea->getEquipo() != null) ? $linea->getEquipo()->getModelo()->getNombre() : "Sin equipo asociado",
                        ($linea->getEmpleado() != null) ? $linea->getEmpleado()->getCodeCeco() : "Empleado no asociado",
                        ($linea->getPlan() != null) ? $linea->getPlan()->getCodigo() : "Sin plan asociado",
                        ($linea->getPlan() != null) ? $linea->getPlan()->getNombre() : "Sin plan asociado",
                        ($linea->getPlan() != null) ? $linea->getPlan()->getCosto() : "Sin plan asociado",
                        ($linea->getEquipo() != null) ? $linea->getEquipo()->getImei() : "Sin equipo asociado",
                        ($linea->getGrupo() != null) ? $linea->getGrupo()->getNombre() : "General",
                        $restantes,
                        $restantes * (($linea->getEquipo() != null) ? $linea->getEquipo()->getModelo()->getCuotaArrendamiento() : 0 ) 
                    );
                }
                $excel = $excel->writeExcel($l);
                $this->addFlash("Exito","Se creó informe <a  target='_blank' href='uploads/writer/export.xlsx' >Click Aqui</a> para descargar.");
        }
        return $this->render('informe/index.html.twig', [
            'formularios' => array(
                'ceco' => $ceCoForm->createView(),
                'empleado'=> $empleadoForm->createView(),
                'linea'=> $lineasForm->createView(),
            ),
            'controller_name' => 'InformeController',
        ]);
    }
}
