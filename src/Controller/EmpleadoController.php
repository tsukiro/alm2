<?php

namespace App\Controller;

use App\Entity\Empleado;
use App\Form\EmpleadoType;
use App\Form\ImportExcelType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\excelManagementService;
use App\Service\FileUploader;

/**
 * Controlador de la vista de empleados
 * Route : Define la ruta web desde donde se accederá a la vista, Ej. (http://alm.raion.cl/empleado) 
 * @Route("/empleado")
 */
class EmpleadoController extends Controller
{
    /**
     * @Route("/", name="empleado_index", methods="GET")
     */
    public function index(): Response
    {   
        $empleados = $this->getDoctrine()->getRepository(Empleado::class)->findAll();
        return $this->render('empleado/index.html.twig', array (
            'empleados' => $empleados,
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'empleado_index',
                  'title' => 'Empleado',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
         ));
    }

    /**
     * @Route("/new", name="empleado_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $empleado = new Empleado();
        $form = $this->createForm(EmpleadoType::class, $empleado);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $empleado = $form->getData();
            
            $empleado->setFechaIngreso(new \DateTime($empleado->getFechaIngreso()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($empleado);
            $em->flush();
              $this->addFlash("Exito","Se ha creado nuevo empleado correctamente"); 
              return $this->redirectToRoute('administacion',array("active"=>"empleados"));
        
              //$this->addFlash("Error","No se la logrado crear el nuevo empleado");
           
        }

        return $this->render('empleado/new.html.twig', [
            'empleado' => $empleado,
            'form' => $form->createView(),
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'empleado_index',
                  'title' => 'Empleado',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
        ]);
    }

    /**
     * @Route("/{id}", name="empleado_show", methods="GET")
     */
    public function show(Empleado $empleado): Response
    {
        
        if ($empleado != null){
            
            return $this->render('empleado/show.html.twig',
            ['empleado' => $empleado,
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'empleado_index',
                  'title' => 'Empleado',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
            ]);
        }else{
            $this->addFlash("error","No se encontro el empleado");
            return $this->redirectToRoute('administacion',array("active"=>"empleados"));
        }
        
    }

    /**
     * @Route("/edit/{id}/", name="empleado_edit", methods="GET|POST|PUT")
     */
    public function edit(Empleado $empleado,Request $request): Response
    {
        if ($empleado != null){
            dump($empleado->getFechaIngreso());
            $form = $this->createForm(EmpleadoType::class, $empleado);
            $form->handleRequest($request);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $empleado = $form->getData();
            
            $empleado->setFechaIngreso(new \DateTime($empleado->getFechaIngreso()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($empleado);
            $em->flush();
            $this->addFlash("Exito","Se ha modificado empleado correctamente"); 
            return $this->redirectToRoute('administacion',array("active"=>"empleados"));
        }

        return $this->render('empleado/edit.html.twig', [
            'empleado' => $empleado,
            'form' => $form->createView(),
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'empleado_index',
                  'title' => 'Empleado',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
        ]);
    }

    /**
     * @Route("/{id}", name="empleado_delete", methods="DELETE")
     */
    public function delete( Empleado $empleado, Request $request): Response
    {
        if ($empleado != null){
            $empleadoData = json_decode($data->raw_body);
            $empleado = $converter->convertToClass($empleadoData, Empleado::class);

            if ($this->isCsrfTokenValid('delete'.$empleado->getId(), $request->request->get('_token'))) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($empleado);
                $em->flush();
                $this->addFlash("Exito","Se ha eliminado correctamente el empleado");
                // $this->addFlash("Exito","No se encontró ningún empleado con ese ID");
            }
        }
        return $this->redirectToRoute('administacion',array("active"=>"empleados"));
    }

    /**
     * @Route("/import/",name="importExcelEmpleado")
     */
    public function import(Request $request, excelManagementService $excel, FileUploader $fileUploader){

        $form = $this->createForm(ImportExcelType::class,array(
            'action' => $this->generateUrl('importExcelEmpleado'),
        ));

        $form->handleRequest($request);
        
        $empleados = array();
        if ($form->isSubmitted() && $form->isValid()){
            $file = $form["file"]->getData();
            $fileName = $fileUploader->upload($file);
            $archivo = $excel->readExcel("uploads/imports/import.xlsx");
            $items = count($archivo);
            $editados = 0;
            $creados = 0;
            $count = 0;
            foreach ($archivo as $row){
                if ($count == 0){
                    $count++;
                }else{
                    
                    $empleado = $this->getDoctrine()->getRepository(Empleado::class)->findByRut($row[0]);
                    
                    $em = $this->getDoctrine()->getManager();
                    if ($empleado != null){
                        $empleado->setNombre($row[1]);
                        $empleado->setFechaIngreso($row[2]);
                        $empleado->setCodeCeco($row[3]);
                        $empleado->setNombreCeco($row[4]);
                        $empleado->setUnidadOrganizativa($row[5]);
                        $empleado->setDivision($row[6]);
                        $empleado->setSubdivision($row[7]);
                        $empleado->setCargo($row[8]);
                        $empleado->setEmpresa($row[9]);
                        if ($row[10] == null){
                            $row[10] = 'Jefatura no ingresada';
                        }
                        $empleado->setNombreJefatura($row[10]);
                        $em->persist($empleado);
                        $empleados[] = $empleado;
                        $editados++;
                    }else{
                        $empleado = new Empleado();
                        $empleado->setRut($row[0]);
                        $empleado->setNombre($row[1]);
                        $empleado->setFechaIngreso($row[2]);
                        $empleado->setCodeCeco($row[3]);
                        $empleado->setNombreCeco($row[4]);
                        $empleado->setUnidadOrganizativa($row[5]);
                        $empleado->setDivision($row[6]);
                        $empleado->setSubdivision($row[7]);
                        $empleado->setCargo($row[8]);
                        $empleado->setEmpresa($row[9]);
                        if ($row[10] == null){
                            $row[10] = 'Jefatura no ingresada';
                        }
                        $empleado->setNombreJefatura($row[10]);
                        $em->persist($empleado);
                        
                        $creados++;
                    }
                    $em->flush();
                }
              }
              $this->addFlash("Exito","Se han creado ".$creados." empleados y modificado ".$editados." de ".$items." items importados.");
              
        }
        return $this->redirectToRoute("gestion",["active"=>"empleados"]);
    }
}
