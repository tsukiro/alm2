<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Grupo;
use Symfony\Component\HttpFoundation\Request;
use App\Form\GrupoType;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/grupo")
 */
class GrupoController extends Controller
{
    /**
     * @Route("/", name="grupo_index")
     */
    public function index()
    {
        $grupos = $this->getDoctrine()->getRepository(Grupo::class)->findAll();
        return $this->render('grupo/index.html.twig', [
            'controller_name' => 'Grupo',
            'grupos' => $grupos,
        ]); 
    }
    
    /**
     * @Route("/new", name="grupo_new", methods="GET|POST")
     */
    public function new(Request $request  ): Response
    {   
     
        $grupo = new Grupo();
        $form = $this->createForm(GrupoType::class, $grupo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $grupo = $form->getData();
            $grupo->setLineas(array());
            $em = $this->getDoctrine()->getManager();
            $em->persist($grupo);
            $data = $em->flush();
            dump($data,$this);
            $this->addFlash("Exito","Se ha creado nuevo grupo correctamente"); 
            return $this->redirectToRoute('administracion',array("active" => "grupos"));
          
           //$this->addFlash("Error","No se la logrado crear el nuevo grupo");
           
        }

        return $this->render('grupo/new.html.twig', [
            'grupo' => $grupo,
            'active' => array("grupos" => true),
            'form' => $form->createView(),
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'grupo_index',
                  'title' => 'Grupo',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
        ]);
    }

    /**
     * @Route("/{id}", name="grupo_show", methods="GET")
     */
    public function show(Grupo $grupo): Response
    {
        if ($grupo != null){
            return $this->render('grupo/show.html.twig',
            ['grupo' => $grupo,
                'active' => array("grupos" => true),
                'page' => array(
                    'title' => '',
                    'description' => '',
                    'module' => array(
                    'name' => 'grupo_index',
                    'title' => 'Grupo',
                    'icon' => 'fa-list-alt'
                    ),
                ),
            ]);
        }else{
            $this->addFlash("error","No se encontro el grupo");
            return $this->redirectToRoute("administracion");
        }
    }

    /**
     * @Route("/{id}/edit", name="grupo_edit", methods="GET|POST")
     */
    public function edit(Grupo $grupo,  Request $request ): Response
    {

        if ($grupo != null){
            $form = $this->createForm(GrupoType::class, $grupo);
            $form->handleRequest($request);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $grupo = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($grupo);
            $em->flush();

            $this->addFlash("Exito","Se ha modificado grupo correctamente"); 
            return $this->redirectToRoute('administracion', ['active' => "grupos"]);
            
            //$this->addFlash("Error","Hubo un error al intentar editar el grupo.");
            //return $this->redirectToRoute("grupo_index");
            
        }

        return $this->render('grupo/edit.html.twig', [
            
            'active' => array("grupos" => true),
            'grupo' => $grupo,
            'form' => $form->createView(),
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'grupo_index',
                  'title' => 'Grupo',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
        ]);
    }

    /**
     * @Route("/{id}", name="grupo_delete", methods="DELETE")
     */
    public function delete(Request $request, Grupo $grupo): Response
    {
        if ($grupo != null){
            
            if ($this->isCsrfTokenValid('delete'.$grupo->getId(), $request->request->get('_token'))) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($grupo);
                $em->flush();
                    $this->addFlash("Exito","Se ha eliminado correctamente el grupo");
                
                //    $this->addFlash("Exito","No se encontró ningún grupo con ese ID");
                
            }
        }

        return $this->redirectToRoute('administracion');
    }
}
