<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Linea;
use App\Entity\Equipo;
use App\Form\ImportExcelType;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;
use App\Entity\Empleado;

class GestionController extends Controller
{
    /**
     * @Route("/gestion/{active}", name="gestion")
     */
    public function index(Request $request,$active = "lineas")
    {
        $lineas = $this->getDoctrine()->getRepository(Linea::class)->findAll();
        $equipos = $this->getDoctrine()->getRepository(Equipo::class)->findAll();
        $empleados = $this->getDoctrine()->getRepository(Empleado::class)->findAll();
        $empleadosForm = $this->createForm(ImportExcelType::class,null ,array(
            'action' => $this->generateUrl('importExcelEmpleado'),
        ));
        $empleadosForm->handleRequest($request);

        $lineasForm = $this->createForm(ImportExcelType::class,null ,array(
            'action' => $this->generateUrl('importExcelLinea'),
        ));
        $lineasForm->handleRequest($request);

        $equiposForm = $this->createForm(ImportExcelType::class,null ,array(
            'action' => $this->generateUrl('importExcelEquipo'),
        ));
        $equiposForm->handleRequest($request);

        $modelosForm = $this->createForm(ImportExcelType::class,null ,array(
            'action' => $this->generateUrl('importExcelModelo'),
        ));
        $modelosForm->handleRequest($request);


        return $this->render('gestion/index.html.twig', [
            "active" => array($active => true),
            'controller_name' => 'Gestion',
            'lineas' => $lineas,
            'equipos' => $equipos,
            'empleados' => $empleados,
            'formularios' => array(
                'empleados' => $empleadosForm->createView(),
                'lineas' => $lineasForm->createView(),
                'equipos' => $equiposForm->createView(),
                'modelos' => $modelosForm->createView(),
            ),
        ]);
    }
}
