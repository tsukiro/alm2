<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\User;
use App\Entity\Role;
use App\Entity\Grupo;
use App\Entity\Modelo;
use App\Entity\Plan;


class AdministracionController extends Controller
{
    /**
     * @Route("/administracion/{active}", name="administracion")
     */
    public function index($active = "usuarios")
    {

        $usuarios = $this->getDoctrine()->getRepository(User::class)->findAll();
        $grupos = $this->getDoctrine()->getRepository(Grupo::class)->findAll();
        $modelos = $this->getDoctrine()->getRepository(Modelo::class)->findAll();
        $planes = $this->getDoctrine()->getRepository(Plan::class)->findAll();

        return $this->render('administracion/index.html.twig', [
            "active" => array($active => true),
            'controller_name' => 'Administracion',
            'usuarios' => $usuarios,
            'grupos' => $grupos,
            'modelos' => $modelos,
            'planes' => $planes,
        ]);
    }
}
