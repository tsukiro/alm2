<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UsuarioType;
use App\Form\PerfilType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\ChangePasswordType;

/**
 * @Route("/usuario")
 */
class UsuarioController extends Controller
{
    /**
     * @Route("/", name="usuario_index", methods="GET")
     */
    public function index() : Response
    {
        $usuarios = $em->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('usuario/index.html.twig', array (
            'usuarios' => $usuarios,
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'usuario_index',
                  'title' => 'Usuario',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
         ));
    }

    /**
     * @Route("/new", name="usuario_new", methods="GET|POST")
     */
    public function new(Request $request,UserPasswordEncoderInterface $passwordEncoder) : Response
    {
        $usuario = new User();
        $form = $this->createForm(UsuarioType::class, $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usuario = $form->getData();
            $tempPassword = "Temp".date("dmY");
            $usuario->setPassword($tempPassword);
            $usuario->setPassword($passwordEncoder->encodePassword($usuario,$usuario->getPassword()));
            $usuario->setChangePassword(true);
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();
            $this->addFlash("Exito","Se ha creado nuevo usuario con la contraseña : ".$tempPassword);     
            return $this->redirectToRoute('administracion',array("active"=>"usuarios"));
           
               /* $this->addFlash("Error","No se la logrado crear el nuevo usuario.");
                return $this->redirectToRoute('usuario_index');*/
            
        }

        return $this->render('usuario/new.html.twig', [
            'usuario' => $usuario,
            'form' => $form->createView(),
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'usuario_index',
                  'title' => 'Usuario',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
        ]);
    }
    /**
     * @Route("/{id}", name="usuario_show", methods="GET")
     */
    public function show(User $usuario) : Response
    {
        if ($usuario != null){
            return $this->render('usuario/show.html.twig',
            ['usuario' => $usuario,
                'page' => array(
                    'title' => '',
                    'description' => '',
                    'module' => array(
                    'name' => 'usuario_index',
                    'title' => 'Usuario',
                    'icon' => 'fa-list-alt'
                    ),
                ),
            ]);
        }else{
            $this->addFlash("Error","No se encontro el usuario");
            return $this->redirectToRoute('administracion',array("active"=>"usuarios"));
        }
    }
    /**
     * @Route("/edit/{id}/", name="usuario_edit", methods="GET|POST|PUT")
     */
    public function edit(User $usuario,Request $request) : Response
    {
        if ($usuario != null){
            $form = $this->createForm(UsuarioType::class, $usuario);
            $form->handleRequest($request);
        }else{
            $this->addFlash("Error","No se encontró el usuario solicitado.");
            return $this->redirectToRoute('administracion',array("active"=>"usuarios"));
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $usuario = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();
            
                $this->addFlash("Exito","Se ha modificado usuario correctamente"); 
                return $this->redirectToRoute('administracion',array("active"=>"usuarios"));
          
               //$this->addFlash("Error","Hubo un error al intentar editar el usuario.");
               // return $this->redirectToRoute("usuario_index");
            
        }
        return $this->render('usuario/edit.html.twig', [
            'usuario' => $usuario,
            'form' => $form->createView(),
            'page' => array(
                'title' => '',
                'description' => '',
                'module' => array(
                  'name' => 'usuario_index',
                  'title' => 'Usuario',
                  'icon' => 'fa-list-alt'
                ),
 
              ),
        ]);
        
    }
    /**
     * @Route("/{id}", name="usuario_delete", methods="DELETE")
     */
    public function delete(User $usuario, Request $request) : Response
    {
           if ($this->isCsrfTokenValid('delete'.$usuario->getId(), $request->request->get('_token'))) {
               $em = $this->getDoctrine()->getManager();
               $em->remove($usuario);
               $em->flush();
            
                $this->addFlash("Exito","Usuario eliminado correctamente");
            //    $this->addFlash("Error","Hubo un error al eliminar el usuario.");
            
        }

        return $this->redirectToRoute('administracion',array("active"=>"usuarios"));
    }
    /**
     * @Route("/{id}", name="usuario_reset_password", methods="POST")
     */
    public function resetPassword(User $usuario, Request $request, UserPasswordEncoderInterface $passwordEncoder) : Response
    {
        if ($this->isCsrfTokenValid('reset'.$usuario->getId(), $request->request->get('_token'))) {
           
            
            $tempPassword = "Temp".date("dmY");
            $usuario->setPassword($tempPassword);
            $usuario->setPassword($passwordEncoder->encodePassword($usuario,$usuario->getPassword()));
            $usuario->setChangePassword(true);
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();
            $this->addFlash("Exito", "Se ha creado la nueva contraseña : ".$tempPassword);
        }
        return $this->redirectToRoute('administracion',array("active"=>"usuarios"));
    }

    /**
     * @Route("/{id}/password/change", name="resetPassword")
     */
    public function passwordChange(User $usuario, Request $request, UserPasswordEncoderInterface $passwordEncoder){
        
        if ($usuario != null){
            
            $form = $this->createForm(ChangePasswordType::class,$usuario);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()){   

                $usuario->setPassword($passwordEncoder->encodePassword($usuario,$usuario->getPassword()));
                $usuario->setChangePassword(false);
                $em = $this->getDoctrine()->getManager();
                $em->persist($usuario);
                $em->flush();
                $this->addFlash("Exito", "Se ha modificado la contraseña exitosamente");
                return $this->redirectToRoute('welcome');
            }

            return $this->render('usuario/changePassword.html.twig', [
                'usuario' => $usuario,
                'form' => $form->createView(),
                'page' => array(
                    'title' => '',
                    'description' => '',
                    'module' => array(
                      'name' => 'usuario_index',
                      'title' => 'Usuario',
                      'icon' => 'fa-list-alt'
                    ),
     
                  ),
            ]);
        }

    }
}

