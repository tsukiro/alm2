<?php

namespace App\Service;

class dateManager{
    public function get_month_diff($start, $end = FALSE)
    {
        $end OR $end = date("Y-m-d",strtotime("now"));
        //$start = new \DateTime("$start");
        
        $end   = new \DateTime("$end");
        $diff  = $start->diff($end);
        return $diff->format('%y') * 12 + $diff->format('%m');
    }
}