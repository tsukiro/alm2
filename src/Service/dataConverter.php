<?php

namespace App\Service;
use App\Entity\Empleado;
use App\Entity\Usuario;
use App\Entity\Linea;
use App\Entity\Role;

class dataConverter{

    private $provider;
    
    function __construct( almApiDataProvider $provider){
        
        $this->provider = $provider;
    }
    function isSubClass($subClassName){
        
        if (is_array($subClassName) && count($subClassName) > 0){
            $val = explode("/",$subClassName[0]);
            if (count($val) == 3){
                switch ($val[1]){
                    case "usuarios":return true;break;
                    case "empleados":return true;break;
                    case "modelos":return true;break;
                    case "grupos":return true;break;
                    case "equipos":return true;break;
                    case "lineas":return true;break;
                    case "roles":return true;break;
                    default: false; break;
                }
            }

        }else if (is_array($subClassName) && count($subClassName) <= 0)
        {   
            return true;
        }
        else
        {
        $val = explode("/",$subClassName);
        if (count($val) == 3){
            switch ($val[1]){
                case "usuarios":return true;break;
                case "empleados":return true;break;
                case "modelos":return true;break;
                case "grupos":return true;break;
                case "equipos":return true;break;
                case "lineas":return true;break;
                case "roles":return true;break;
                default: false; break;
            }
        }
        }
        
    }
    function convertToClass($a, $className){
        $c = new $className;
        foreach($a as $attr => $value){
            if ($this->isSubClass($value)){
                if (is_array($value) && count($value) > 0){
                    
                    $data = $this->provider->getSubResource($value[0]);
                    
                }else if(is_array($value) && count($value) <= 0){
                    
                    $c->{"set".ucfirst($attr)}(array());
                }else{
                    $data = $this->provider->getSubResource($value);
                    
                }

            
                if (isset($data->code) && $data->code != 404 ){
                    if ($attr != "lineas" && $attr != "usuarios" ){
                        if ($attr == "rol"){
                            $c->{"set".ucfirst($attr)}($this->convertToClass($data->body,"App\Entity\\".ucfirst("role")));
                        }else{
                            $c->{"set".ucfirst($attr)}($this->convertToClass($data->body,"App\Entity\\".ucfirst($attr)));
                            if ($attr == "equipo"){
                                $c->getEquipo()->setModelo(
                                    $this->convertToClass(
                                    $this->provider->getSubResource($c->getEquipo()->getModelo())->body,
                                    "App\Entity\Modelo"
                                    )
                                );
                            }
                        }
                        
                        
                    }
                    
                }else{
                    $c->{"set".ucfirst($attr)}(null);
                }
                
            }else{
                $c->{"set".ucfirst($attr)}($value);
            }
        }
        return $c;
    }
    function convertToArray($c){
        if (is_array($c) || is_object($c))
        {
            $result = array();
            foreach ($c as $key => $value)
            {
                $result[$key] = $this->convertToArray($value);
            }
            return $result;
        }
        return $c;
    }
}