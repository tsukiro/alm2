<?php

namespace App\Service;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class excelManagementService{

    public function readExcel($file){
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($file);
        $worksheet = $spreadsheet->getAllSheets();
        return $worksheet[0]->toArray();
    }
    public function writeExcel($data){
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $cols = array(
            "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P"
        );
        $colsMax = count($data[0]);
        $rowCount = 1;
        foreach($data as $row){
            for ($i=0; $i < $colsMax; $i++) { 
                $sheet->setCellValue($cols[$i].$rowCount, $row[$i]);
            }
            $rowCount++;
        }
        
        
        $writer = new Xlsx($spreadsheet);
        $writer->save('uploads/writer/export.xlsx');

    }
}