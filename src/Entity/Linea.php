<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Dtc\GridBundle\Annotation as Grid;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Lineas telefonicas de la empresa
 * Class Linea
 * @Grid\Grid(actions={@Grid\ShowAction(), @Grid\DeleteAction()})
 * @ORM\Entity(repositoryClass="App\Repository\LineaRepository")
 * @UniqueEntity("numero", message="Ya se encuentra registrado este número en el sistema")
 */
class Linea
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
    * Numero de la linea
    *
    * @Assert\NotBlank()
    * @Assert\Length(
     *      min = 9,
     *      max = 9,
     *      minMessage = "Debe contener exactamente 9 números Ej. 991234567",
     *      maxMessage = "Debe contener exactamente 9 números Ej. 991234567"
     * )
    * @ORM\Column(type="integer")
    */
    private $numero;

    /**
     * Empleado asociado
     * @var App\Entity\Empleado
     * @ORM\ManyToOne(targetEntity="App\Entity\Empleado", inversedBy="lineas")
     */
    private $empleado;

    /**
     * Grupo de la linea 
     * @var App\Entity\Grupo
     * @ORM\ManyToOne(targetEntity="App\Entity\Grupo", inversedBy="lineas")
     */
    private $grupo;
    /**
     * Grupo de la linea 
     * @var App\Entity\Equipo
     * @ORM\ManyToOne(targetEntity="App\Entity\Equipo", inversedBy="lineas")
     */
    private $equipo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Plan", inversedBy="lineas")
     */
    private $plan;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Movimiento", mappedBy="linea")
     */
    private $movimientos;

    public function __construct()
    {
        $this->plan = new ArrayCollection();
        $this->movimientos = new ArrayCollection();
    }

    

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get empleado asociado
     *
     * @return  App\Entity\Empleado
     */ 
    public function getEmpleado()
    {
        return $this->empleado;
    }

    /**
     * Set empleado asociado
     *
     * @param  App\Entity\Empleado  $empleado  Empleado asociado
     *
     * @return  self
     */ 
    public function setEmpleado($empleado)
    {
        $this->empleado = $empleado;

        return $this;
    }

    /**
     * Get grupo de la linea
     *
     * @return  App\Entity\Grupo
     */ 
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * Set grupo de la linea
     *
     * @param  App\Entity\Grupo  $grupo  Grupo de la linea
     *
     * @return  self
     */ 
    public function setGrupo( $grupo)
    {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * Get grupo de la linea
     *
     * @return  App\Entity\Equipo
     */ 
    public function getEquipo()
    {
        return $this->equipo;
    }

    /**
     * Set grupo de la linea
     *
     * @param  App\Entity\Equipo  $equipo  Grupo de la linea
     *
     * @return  self
     */ 
    public function setEquipo( $equipo)
    {
        $this->equipo = $equipo;

        return $this;
    }

    /**
     * Get numero de la linea
     */ 
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set numero de la linea
     *
     * @return  self
     */ 
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * @return Collection|Plan[]
     */
    public function getPlan()
    {
        return $this->plan;
    }
    public function setPlan(Plan $plan){
        $this->plan = $plan;
        return $this;
    }

    /*
    public function addPlan(Plan $plan): self
    {
        if (!$this->plan->contains($plan)) {
            $this->plan[] = $plan;
            $plan->setLineas($this);
        }

        return $this;
    }

    public function removePlan(Plan $plan): self
    {
        if ($this->plan->contains($plan)) {
            $this->plan->removeElement($plan);
            // set the owning side to null (unless already changed)
            if ($plan->getLineas() === $this) {
                $plan->setLineas(null);
            }
        }

        return $this;
    }*/

    /**
     * @return Collection|Movimiento[]
     */
    public function getMovimientos(): Collection
    {
        return $this->movimientos;
    }

    public function addMovimiento(Movimiento $movimiento): self
    {
        if (!$this->movimientos->contains($movimiento)) {
            $this->movimientos[] = $movimiento;
            $movimiento->setLinea($this);
        }

        return $this;
    }

    public function removeMovimiento(Movimiento $movimiento): self
    {
        if ($this->movimientos->contains($movimiento)) {
            $this->movimientos->removeElement($movimiento);
            // set the owning side to null (unless already changed)
            if ($movimiento->getLinea() === $this) {
                $movimiento->setLinea(null);
            }
        }

        return $this;
    }
}
