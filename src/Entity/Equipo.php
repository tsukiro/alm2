<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * 
 * 
 * @ORM\Entity(repositoryClass="App\Repository\EquipoRepository")
 */
class Equipo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * IMEI del equipo
     *
     * @var string $imei
     * @ORM\Column(type="string", nullable=false)
     */
    private $imei;
    
    /**
     * Fecha de Ingreso del equipo
     * 
     * @var Datetime $fechaIngreso
     * @ORM\Column(type="datetime",nullable=false)
     */
    private $fechaIngreso;

    /**
     * Modelo del equipo
     * @ORM\ManyToOne(targetEntity="App\Entity\Modelo", inversedBy="equipos")
     */
    private $modelo;
    /**
     * Lineas asociadas a un equipo
     * @var App\Entity\Linea
     * @ORM\OneToMany(targetEntity="App\Entity\Linea", mappedBy="equipo")
     */
    private $lineas;

    /**
     * @ORM\Column(type="integer")
     */
    private $estado;

    function __construct()
    {
        $this->lineas = new ArrayCollection();
    }
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get $imei
     *
     * @return  string
     */ 
    public function getImei()
    {
        return $this->imei;
    }

    /**
     * Set $imei
     *
     * @param  string  $imei  $imei
     *
     * @return  self
     */ 
    public function setImei(string $imei)
    {
        $this->imei = $imei;

        return $this;
    }

    /**
     * Get $fechaIngreso
     *
     * @return  Datetime
     */ 
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Set $fechaIngreso
     *
     * @param  Datetime  $fechaIngreso  $fechaIngreso
     *
     * @return  self
     */ 
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get modelo
     */ 
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set modelo
     *
     * @return  self
     */ 
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get lineas asociadas a un equipo
     *
     * @return  App\Entity\Linea
     */ 
    public function getLineas()
    {
        return $this->lineas;
    }

    /**
     * Set lineas asociadas a un equipo
     *
     * @param  App\Entity\Linea  $lineas  Lineas asociadas a un equipo
     *
     * @return  self
     */ 
    public function setLineas($lineas)
    {
        $this->lineas = $lineas;

        return $this;
    }
    public function displayName(){
        return $this->imei." - ".$this->modelo->getNombre();
    }

    public function getEstado(): ?int
    {
        return $this->estado;
    }

    public function setEstado(int $estado): self
    {
        $this->estado = $estado;

        return $this;
    }
}
