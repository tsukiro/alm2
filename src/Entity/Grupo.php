<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * 
 * @ORM\Entity(repositoryClass="App\Repository\GrupoRepository")
 */
class Grupo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Nombre del grupo
     *
     * @var string $nombre
     * @ORM\Column(type="string", nullable=false)
     */
    private $nombre;
    /**
     * Lineas asociadas a un grupo
     * @var App\Entity\Linea
     * @ORM\OneToMany(targetEntity="App\Entity\Linea", mappedBy="grupo")
     */
    private $lineas;

    function __construct()
    {
        /**
         * Instancia un Array para almacenar las lineas compatible con Doctrine
         * para mayor referencias ver la documentación de Symfony.com 
         */
        $this->lineas = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get $nombre
     *
     * @return  string
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set $nombre
     *
     * @param  string  $nombre  $nombre
     *
     * @return  self
     */ 
    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get lineas asociadas a un grupo
     *
     * @return  App\Entity\Linea
     */ 
    public function getLineas()
    {
        return $this->lineas;
    }

    /**
     * Set lineas asociadas a un grupo
     *
     * @param  App\Entity\Linea  $lineas  Lineas asociadas a un grupo
     *
     * @return  self
     */ 
    public function setLineas($lineas)
    {
        $this->lineas = $lineas;

        return $this;
    }
}
