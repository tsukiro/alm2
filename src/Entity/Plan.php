<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanRepository")
 */
class Plan
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codigo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Linea", mappedBy="plan")
     */
    private $lineas;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minutos;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $umbral;

    /**
     * @ORM\Column(type="integer")
     */
    private $estado;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Modelo", mappedBy="plan")
     */
    private $modelos;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $costo;

    public function __construct()
    {
        $this->modelos = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getLineas(): ?Linea
    {
        return $this->lineas;
    }

    public function setLineas(?Linea $lineas): self
    {
        $this->lineas = $lineas;

        return $this;
    }

    public function getMinutos(): ?int
    {
        return $this->minutos;
    }

    public function setMinutos(?int $minutos): self
    {
        $this->minutos = $minutos;

        return $this;
    }

    public function getUmbral(): ?int
    {
        return $this->umbral;
    }

    public function setUmbral(?int $umbral): self
    {
        $this->umbral = $umbral;

        return $this;
    }

    public function getEstado(): ?int
    {
        return $this->estado;
    }

    public function setEstado(int $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return Collection|Modelo[]
     */
    public function getModelos(): Collection
    {
        return $this->modelos;
    }

    public function addModelo(Modelo $modelo): self
    {
        if (!$this->modelos->contains($modelo)) {
            $this->modelos[] = $modelo;
            $modelo->setPlan($this);
        }

        return $this;
    }

    public function removeModelo(Modelo $modelo): self
    {
        if ($this->modelos->contains($modelo)) {
            $this->modelos->removeElement($modelo);
            // set the owning side to null (unless already changed)
            if ($modelo->getPlan() === $this) {
                $modelo->setPlan(null);
            }
        }

        return $this;
    }

    public function getCosto(): ?float
    {
        return $this->costo;
    }

    public function setCosto(?float $costo): self
    {
        $this->costo = $costo;

        return $this;
    }
}
