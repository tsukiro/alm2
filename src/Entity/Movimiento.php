<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovimientoRepository")
 */
class Movimiento
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Linea", inversedBy="movimientos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $linea;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Empleado", inversedBy="movimientos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $empleado;

    /**
     * @ORM\Column(type="integer")
     */
    private $estado;

    /**
     * @ORM\Column(type="integer")
     */
    private $tipo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Empleado", inversedBy="movimientosOrigen")
     * @ORM\JoinColumn(nullable=false)
     */
    private $empleadoOrigen;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $observacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="movimientos")
     */
    private $tecnico;

    public function getId()
    {
        return $this->id;
    }

    public function getLinea(): ?Linea
    {
        return $this->linea;
    }

    public function setLinea(?Linea $linea): self
    {
        $this->linea = $linea;

        return $this;
    }

    public function getEmpleado(): ?Empleado
    {
        return $this->empleado;
    }

    public function setEmpleado(?Empleado $empleado): self
    {
        $this->empleado = $empleado;

        return $this;
    }

    public function getEstado(): ?int
    {
        return $this->estado;
    }

    public function setEstado(int $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getTipo(): ?int
    {
        return $this->tipo;
    }

    public function setTipo(int $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getEmpleadoOrigen(): ?Empleado
    {
        return $this->empleadoOrigen;
    }

    public function setEmpleadoOrigen(?Empleado $empleadoOrigen): self
    {
        $this->empleadoOrigen = $empleadoOrigen;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getTipo2(){
        switch($this->tipo){
            case 0:
            return "Nuevo";
            break;
            case 1:
            return "Asignación";
            break;
            case 2:
            return "Retiro";
            break;
            case 3:
            return "Renovación";
            break;
            case 4:
            return "Cesión";
            break;
            case 5:
            return "Baja";
            break;
            case 6:
            return "Cambio Equipo";
            break;
            case 7:
            return "Cambio Numero";
            break;
        }
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

    public function getTecnico(): ?User
    {
        return $this->tecnico;
    }

    public function setTecnico(?User $tecnico): self
    {
        $this->tecnico = $tecnico;

        return $this;
    }
}
