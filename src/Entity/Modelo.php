<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Dtc\GridBundle\Annotation as Grid;

/**
 * @Grid\Grid(actions={@Grid\ShowAction(), @Grid\DeleteAction()})
 * @ORM\Entity(repositoryClass="App\Repository\ModeloRepository")
 */
class Modelo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
     /**
     * Nombre del Modelo
     *
     * @var string $nombre
     * @ORM\Column(type="string", nullable=false)
     */
    private $nombre;
    /**
     * Código del modelo
     *
     * @var string $codigo
     * @ORM\Column(type="string", nullable=false)
     */
    private $codigo;

    /**
     * Equipos asociados a este modelo
     * @ORM\OneToMany(targetEntity="App\Entity\Equipo", mappedBy="modelo")
     */
    private $equipos;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $descuento;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codDescuento;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Plan", inversedBy="modelos")
     */
    private $plan;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $precioVenta;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cuotaInicial;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cuotaArrendamiento;
    function __construct()
    {
        $this->equipos = new ArrayCollection();
    }
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get $nombre
     *
     * @return  string
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set $nombre
     *
     * @param  string  $nombre  $nombre
     *
     * @return  self
     */ 
    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get $codigo
     *
     * @return  string
     */ 
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set $codigo
     *
     * @param  string  $codigo  $codigo
     *
     * @return  self
     */ 
    public function setCodigo(string $codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getDescuento(): ?int
    {
        return $this->descuento;
    }

    public function setDescuento(?int $descuento): self
    {
        $this->descuento = $descuento;

        return $this;
    }

    public function getCodDescuento(): ?string
    {
        return $this->codDescuento;
    }

    public function setCodDescuento(string $codDescuento): self
    {
        $this->codDescuento = $codDescuento;

        return $this;
    }

    public function getPlan(): ?Plan
    {
        return $this->plan;
    }

    public function setPlan(?Plan $plan): self
    {
        $this->plan = $plan;

        return $this;
    }

    public function getPrecioVenta(): ?int
    {
        return $this->precioVenta;
    }

    public function setPrecioVenta(?int $precioVenta): self
    {
        $this->precioVenta = $precioVenta;

        return $this;
    }

    public function getCuotaInicial(): ?int
    {
        return $this->cuotaInicial;
    }

    public function setCuotaInicial(?int $cuotaInicial): self
    {
        $this->cuotaInicial = $cuotaInicial;

        return $this;
    }

    public function getCuotaArrendamiento(): ?int
    {
        return $this->cuotaArrendamiento;
    }

    public function setCuotaArrendamiento(?int $cuotaArrendamiento): self
    {
        $this->cuotaArrendamiento = $cuotaArrendamiento;

        return $this;
    }
}
