<?php

namespace App\Form;

use App\Entity\Empleado;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class EmpleadoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rut',TextType::class,array(
                "label" => "Rut"
            ))
            ->add('nombre')
            ->add('codeCeco')
            ->add('nombreCeco')
            ->add('unidadOrganizativa')
            ->add('division')
            ->add('subdivision')
            ->add('cargo')
            ->add('empresa')
            ->add('nombreJefatura')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Empleado::class,
        ]);
    }
}
