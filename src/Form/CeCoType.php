<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CeCoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('valor',TextType::class,array(
            "label" => "Indica centro de costos",
            "attr" => array("placeholder" => "Ej. IF6001")
        ))
        ->add("save",SubmitType::class,array(
            "label" => "Enviar",
            "attr" => array("class" => "btn btn-primary")
        ))
    ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            
        ]);
    }
}
