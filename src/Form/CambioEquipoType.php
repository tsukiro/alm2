<?php

namespace App\Form;

use App\Entity\Linea;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Empleado;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use App\Entity\Equipo;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CambioEquipoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
       
            ->add('equipo', TextType::class,array(
                "label" => "IMEI del nuevo equipo a asignar",
            ))
            ->add('save',SubmitType::class,array(
                "label" => "Cambiar",
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
          //  'data_class' => Linea::class,
        ]);
    }
}
