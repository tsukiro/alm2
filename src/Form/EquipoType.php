<?php

namespace App\Form;

use App\Entity\Equipo;
use App\Entity\Modelo;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class EquipoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imei')
            ->add('fechaIngreso',DateType::class,array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'input' => "string",
            ))
            ->add('modelo',EntityType::class,array(
                "label" => "Modelo",
                "class" => Modelo::class,
                "choice_label" => "nombre",
            ))
            ->add('estado',ChoiceType::class,array(
                "label" => "Estado",
                "choices" => array(
                    "Activo" => 0,
                    "De Baja" => 1,
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Equipo::class,
        ]);
    }
}
