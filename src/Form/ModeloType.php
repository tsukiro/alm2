<?php

namespace App\Form;

use App\Entity\Modelo;
use App\Entity\Plan;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;


class ModeloType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('codigo')
            ->add('codDescuento')
            ->add('descuento',PercentType::class,array(
                'type' => 'integer',
            ))
            ->add('plan',EntityType::class,array(
                "class" => Plan::class,
                "choice_label" => "nombre",
            ))
            ->add('precioVenta',MoneyType::class,array(
                "currency" => "CLP",
            ))
            ->add('cuotaInicial',MoneyType::class,array(
                "currency" => "CLP",
            ))
            ->add('cuotaArrendamiento',MoneyType::class,array(
                "currency" => "CLP",
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Modelo::class,
        ]);
    }
}
