<?php

namespace App\Form;

use App\Entity\Linea;
use App\Entity\Empleado;
use App\Entity\Grupo;
use App\Entity\Equipo;
use App\Entity\Plan;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class LineaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero',NumberType::class,array(
                "label" => "Linea nueva",
            ))
            ->add('empleado',EntityType::class,array(
                "label" => "Empleado a asignar",
                "class" => Empleado::class,
                "choice_label" => "nombre",
                
            ))
            ->add('grupo',EntityType::class,array(
                "label" => "Grupo a asignar",
                "choice_label" => "nombre",
                "class" => Grupo::class,
            ))
            ->add('equipo',EntityType::class,array(
                "label" => "Equipo a asignar",
                "choice_label" => "displayName",
                "class" => Equipo::class,
            ))
            ->add("plan",EntityType::class,array(
                "label" => "Plan a asignar",
                "choice_label" => "nombre",
                "class" => Plan::class,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Linea::class,
        ]);
    }
}
