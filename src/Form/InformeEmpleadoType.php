<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class InformeEmpleadoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('valor',TextType::class,array(
            "label" => "Ingresa rut de empleado",
            "attr" => array("placeholder" => "Ej. 123456789-0")
        ))
        ->add("save",SubmitType::class,array(
            "label" => "Enviar",
          "attr" => array("class" => "btn btn-primary")
        ))
    ;  
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
